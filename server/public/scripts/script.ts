let io;
let socket = io(),
    canvas = document.getElementById("canvas"),
    context = (canvas as any).getContext("2d"),
    canvasWidth = (canvas as any).width,
    canvasHeight = (canvas as any).height;

context.fillStyle = "red";
context.fillRect(canvasWidth / 2, canvasHeight / 2, 5, 5);

socket.on("whois", (result) => {
   socket.emit("whois response", ClientType.WEB);
});
socket.on("ping result", (result) => {
    console.log("Un résultat a été reçu !");
    console.log(result);
    //ajout des coordonnées des obstacles sur le canvas
    //for(let i = 0; i < result.length; i++){
        context.fillStyle = "yellow";
        context.fillRect(result/*[i]*/.x + canvasWidth / 2, result/*[i]*/.y + canvasHeight / 2, 5, 5);
    ///}
});
socket.on("new position", (position) => {
    console.log("Les données de déplacement ont été reçues");
    //calcule les coordonnées actuelles du robot
    context.fillStyle = "red";
    context.fillRect(position.x + canvasWidth / 2, position.y + canvasHeight / 2, 2, 2);
});

enum ClientType{
    BRICK,
    WEB
}
