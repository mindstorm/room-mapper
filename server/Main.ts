import * as path from "path";
import * as express from "express";

import HTTPServer from "./Application/HTTPServer";
import IOServer from "./Application/IOServer";
import Logger from "./Application/Logger";

const pi = 3.141526;

/**
 * HTTP SERVER
 */
let httpServer = new HTTPServer();
httpServer.setMiddleware(express.static(path.join(__dirname, "public")));
httpServer.setConfig("views", path.join(__dirname, "views"));
httpServer.setConfig("view engine", "jade");
httpServer.setConfig("trust proxy", 1);
httpServer.setRoute("get", "/", (req, res) => {
    res.render("index.jade");
});

/**
 * IO SERVER
 */
let ioServer = new IOServer(httpServer.getHttpServer());
let coordBrick = {x: 0, y: 0};

let brickSocket = undefined;
let webSockets = new Array<SocketIO.Socket>();

ioServer.registerHandler((socket) => {
    socket.emit("whois");
    socket.on("whois response", (data: ClientType) => {
        switch(data){
            case ClientType.BRICK:
                brickHandler();
                break;
            case ClientType.WEB:
                webHandler();
                break;
        }
    });

    function brickHandler(){
        Logger.log("Brick connected !");
        socket.emit("ping request");
        brickSocket = socket;

        socket.on("ping result", (data: PingResult) => {
            Logger.log("Draw position on clients");
            if(data !== undefined){
                Logger.log(`Object : ${JSON.stringify(data, null, 4)}`);
                let objectsCoords: Coord = undefined;
                /*for(let i = 0; i < data.length; i++){
                    objectsCoords[objectsCoords.length] = {
                        x: coordBrick.x + Math.sin(data[i].yaw * pi / 180) * data[i].distance,
                        y: coordBrick.y + Math.cos(data[i].yaw * pi / 180) * data[i].distance
                    };
                }*/
                objectsCoords = {
                        x: coordBrick.x + Math.sin(data.yaw * pi / 180) * data.distance,
                        y: coordBrick.y + Math.cos(data.yaw * pi / 180) * data.distance
                    };

                webSockets.forEach((webSocket: SocketIO.Socket) => {
                    webSocket.emit("ping result", objectsCoords);
                });
            } else {
                Logger.log("No object found !");
            }
        });


/*

        socket.on("ping result", (data: PingResult[]) => {
            Logger.log("Ping result received");
            Logger.log(JSON.stringify(data, null, 4));
            let objectsCoords: Coord[] = new Array<Coord>();
            for(let i = 0; i < data.length; i++){
                objectsCoords.push({
                    x: coordBrick.x + Math.sin(data[i].yaw) * data[i].distance,
                    y: coordBrick.y + Math.cos(data[i].yaw) * data[i].distance
                });
            }
            Logger.log("Draw position on clients");
            webSockets.forEach((webSocket: SocketIO.Socket) => {
                webSocket.emit("ping result", objectsCoords);
            });
        });
        socket.emit("ping request");
        */
    }
    function webHandler(){
        Logger.log("Web client connected !");
        webSockets[webSockets.length] = socket;

        socket.on("disconnect", () => {
           webSockets.splice(webSockets.indexOf(socket), 1);
        });
    }
});

/**
 * START
 */
httpServer.listen(3000, () => {
    ioServer.listen(3001);
});

interface PingResult {
    yaw: number;
    distance: number;
    tick: number;
}
interface Coord {
    x: number;
    y: number;
}
enum ClientType{
    BRICK,
    WEB
}
