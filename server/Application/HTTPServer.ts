import * as express from "express";
import * as path from "path";
import * as http from "http";

import Logger from "./Logger";

class HTTPServer {
    
    private httpApp: express.Express;
    private httpServer: http.Server;
    
    constructor(){
        this.httpApp = express();
        this.httpServer = http.createServer(this.httpApp);
    }
    
    setConfig(key: string, value: any){
        this.httpApp.set(key, value);
    }
    setMiddleware(handler: express.Handler){
        this.httpApp.use(handler);
    }
    setRoute(method: string, route: string, handler: express.RequestHandler){
        this.httpApp[method](route, handler);
    }
    
    getHttpServer(){
        return this.httpServer;
    }
    listen(port: number, callback: () => void){
        this.httpServer.listen(port, () => {
            Logger.log(`HTTP server listening on port ${port} !`);
            callback();
        });
    }
    gracefulExit(){
        this.httpServer.close();
        Logger.log("Gracefully stopped http server");
    }
}
export default HTTPServer;