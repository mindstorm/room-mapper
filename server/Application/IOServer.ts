import * as express from "express";
import * as path from "path";
import * as http from "http";
import * as io from "socket.io";

import Logger from "./Logger";

class IOServer {
    
    private ioServer: SocketIO.Server;
    
    constructor(httpServer: http.Server){
        this.ioServer = io(httpServer);
    }
    
    registerHandler(onConnect: (socket: SocketIO.Socket) => void){
        this.ioServer.on("connection", onConnect);
    }
    
    listen(port: number){
        this.ioServer.listen(port);
        Logger.log(`IO server listening on port ${port} !`);
    }
    gracefulExit(){
        this.ioServer.close();
        Logger.log("Gracefully stopped IO server");
    }
}
export default IOServer;