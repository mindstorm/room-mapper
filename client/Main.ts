import * as sio from "socket.io-client";
let sys = require("sys");
import * as childProcess from "child_process";

let ev3dev = require("ev3dev-lang");

console.log("v29");
console.log("Connecting...");
let socket = sio("http://192.168.173.1:3001");
socket.on("connect", () => {
    console.log("Connected");
});
socket.on("disconnect", () => {
    console.log("Disconnected");
});
socket.on("ping request", (data) => {
    console.log("Receive ping request");

    // Get sensors
    let ultrasonicSensor = new ev3dev.UltrasonicSensor(ev3dev.INPUT_4);
    // Assert sensors
    if(!ultrasonicSensor.connected){
        console.error("No ultrasonic sensor was found on port 4.");
        process.exit(1);
    }

    // Get wheels
    let rightWheel = new ev3dev.Motor(ev3dev.OUTPUT_B);
    let leftWheel = new ev3dev.Motor(ev3dev.OUTPUT_A);
    // Assert wheels
    if(!rightWheel.connected) {
        console.error("No motor was found on port B.");
        process.exit(1);
    }
    if(!leftWheel.connected){
        console.error("No motor was found on port A.");
        process.exit(1);
    }
    // Config wheels
    /*rightWheel.speedRegulationEnabled = "on";
    leftWheel.speedRegulationEnabled = "on";
    rightWheel.command = "run-timed";
    leftWheel.command = "run-timed";
    rightWheel.speedSp = 5;
    leftWheel.speedSp = 5;
    rightWheel.timeSp = 133330;
    leftWheel.timeSp = 133330;
    rightWheel.polarity = "inversed";
    leftWheel.polarity = "normal";*/

    let result = new Array<PingResult>();

    /*let movements = 45;
    function scan(){
        rightWheel.timeSp = 300;
        leftWheel.timeSp = 300;

        setTimeout(() => {
           let distance = ultrasonicSensor.distanceCentimeters;
           if(distance < 50)
                   {yaw: check * 2, distance: distance}
        }, 300);
    }*/


    let check = 0;
    scan();
    function scan(){
        let nbreChecks = 60;
        if(++check > nbreChecks){
            console.log("Ended... send results");
            //socket.emit("ping result", result);
        }else{
            rightWheel.speedRegulationEnabled = "on";
            leftWheel.speedRegulationEnabled = "on";
            rightWheel.command = "run-timed";
            leftWheel.command = "run-timed";
            rightWheel.speedSp = 50;
            leftWheel.speedSp = 50;
            rightWheel.timeSp = 190;
            leftWheel.timeSp = 190;
            rightWheel.polarity = "inversed";
            leftWheel.polarity = "normal";
            let timeout = setTimeout(() => {
                let distance = ultrasonicSensor.distanceCentimeters;
                let distance2 = ultrasonicSensor.distanceCentimeters;
                let distance3 = ultrasonicSensor.distanceCentimeters;
                let middle = (distance + distance2 + distance3) / 3;
                if(middle < 50){
                    console.log(`Add result N${result.length + 1}`);
                    //result[result.length] = {yaw: check * 6.2, distance: middle + 10};
                    socket.emit("ping result", {yaw: check * 6.2, distance: middle + 10});
                }
                clearTimeout(timeout);
                let timeout2 = setTimeout(() => {
                    scan();
                    clearTimeout(timeout2);
                }, 700);
            }, 450);
        }
    }

    /*let check = 0;
    ping();
    function scan(callback: (result: PingResult[]) => void){
        let nbreChecks = 33;
        if(++check > nbreChecks){
            callback(result);
        }else{
            rightWheel.speedRegulationEnabled = "on";
            leftWheel.speedRegulationEnabled = "on";
            rightWheel.command = "run-timed";
            leftWheel.command = "run-timed";
            rightWheel.speedSp = 50;
            leftWheel.speedSp = 50;
            rightWheel.timeSp = 417;
            leftWheel.timeSp = 417;
            rightWheel.polarity = "inversed";
            leftWheel.polarity = "normal";
            let timeout = setTimeout(() => {
                let distance = ultrasonicSensor.distanceCentimeters;
                if(distance < 50){
                    console.log(`Add result N${result.length+1}`);
                    result[result.length] = {yaw: check * 11.25, distance: distance};
                }
                clearTimeout(timeout);
                let timeout2 = setTimeout(() => {
                    scan(callback);
                    clearTimeout(timeout2);
                }, 900);
            }, 450);
        }
    }
    function ping(){
        let pingResults: PingResult[] = new Array<PingResult>();
        scan((results) => {
            check = 0;
            result = new Array<PingResult>();
            scan((results2) => {
                check = 0;
                result = new Array<PingResult>();
                scan((results3) => {
                    let returnResult: PingResult[] = new Array<PingResult>();
                    for(let i = 0; i < results.length; ++i){
                        let manque = 0;
                        let somme = (results[i] ? results[i].distance : manque++) + (results2[i] ? results2[i].distance : manque++) + (results3[i] ? results3[i].distance : manque++);
                        pingResults[pingResults.length] = { yaw: results[i].yaw, distance: somme / (3 - manque)};
                        if(i == results.length - 1){
                            socket.emit("ping result", pingResults);
                            console.log("Ended... send results");
                        }
                    }
                });
            });
        });
    }*/

    /*const interval = 1000;
    let tick = 0;
    let toClearLater = setInterval(() => {
        let angle = 360 * tick / 13333;
        console.log("Angle: " + angle);
        let distance = ultrasonicSensor.distanceCentimeters;
        if(distance < 50)
            result[result.length] = {yaw: angle, distance: distance};
        tick += interval;

        if(angle >= 360){
            console.log("Ended... send results");
            clearInterval(toClearLater);
            socket.emit("ping result", result);
        }
    }, interval);*/
});

socket.on("whois", (result) => {
   socket.emit("whois response", ClientType.BRICK);
});

enum ClientType{
    BRICK,
    WEB
}

interface PingResult {
    yaw: number;
    distance: number;
}
